#pragma once

#include "immintrin.h"

/* Types */

#define u8 unsigned char
#define u64 unsigned long long
#define u256 __m256i

/* Types */


/* Intrinsics */

#define XOR _mm256_xor_si256
#define AND _mm256_and_si256
#define OR _mm256_or_si256
#define NOT(x) _mm256_xor_si256(x, _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) //NOT = XOR between x and something fully filled with 1.
#define NOR(x, y) _mm256_xor_si256(_mm256_or_si256(x, y), _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) // NOR = NOT(OR(x,y)) = OR(x,y) XOR something fully filled with 1.
#define NAND(x,y) _mm256_xor_si256(_mm256_and_si256(x, y), _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) // NAND = NOT(AND(x,y)) = AND(x,y) XOR something fully filled with 1.
#define SHIFTR64(x, y) _mm256_srli_epi64(x, y)
#define SHIFTL64(x, y) _mm256_slli_epi64(x, y)
#define UNPACKHIGH32(x,y) _mm256_unpackhi_epi32(x, y)
#define UNPACKLOW32(x,y) _mm256_unpacklo_epi32(x, y)

#define LOAD(src) _mm256_loadu_si256((__m256i *)(src))
#define STORE(dest,src) _mm256_storeu_si256((__m256i *)(dest),src)

#define SWAPMOVE(a, b, mask, shift)			\
  {							\
    u256 T = AND(XOR(SHIFTL64(a, shift), b), mask);	\
    b = XOR(b, T);					\
    a = XOR(a, SHIFTR64(T, shift));			\
  }

/* Intrinsics */


/* Masks */

#define MASK1 _mm256_set_epi32(0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa)
#define MASK2 _mm256_set_epi32(0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc)
#define MASK4 _mm256_set_epi32(0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0)
#define MASK8 _mm256_set_epi32(0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00)
#define MASK16 _mm256_set_epi32(0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000)
#define MASK32 _mm256_set_epi32(0xffffffff, 0x00000000, 0xffffffff, 0x00000000, 0xffffffff, 0x00000000, 0xffffffff, 0x00000000)

#define MASK1L _mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0xffffffff)
#define MASK3L _mm256_set_epi32(0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000)
#define MASK12L _mm256_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff, 0x00000000, 0x00000000, 0xffffffff, 0xffffffff)

#define RC_64 _mm256_set_epi32(0x000000ff, 0x000000ff, 0x000000ff, 0x0000000ff, 0x000000ff, 0x000000ff, 0x000000ff, 0x000000ff)
#define RC1 _mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff)
#define RC2 _mm256_set_epi32(0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000)
#define RC3 _mm256_set_epi32(0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000)

#define MASK16S _mm256_set_epi32(0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff)

#define MC1 _mm256_set_epi8(31, 30, 29, 28, 19, 18, 17, 16, 27, 26, 25, 24, 23, 22, 21, 20, 15, 14, 13, 12, 3, 2, 1, 0, 11, 10, 9, 8, 7, 6, 5, 4)
#define MC2 _mm256_set_epi8(27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 31, 30, 29, 28, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12)

#define KS_64_1 _mm256_set_epi8(0, 28, 0, 29, 0, 24, 0, 25, 0, 20, 0, 21, 0, 16, 0, 17, 0, 12, 0, 13, 0, 8, 0, 9, 0, 4, 0, 5, 0, 0, 0, 1)
#define KS_64_2 _mm256_set_epi8(29, 0, 31, 0, 25, 0, 27, 0, 21, 0, 23, 0, 17, 0, 19, 0, 13, 0, 15, 0, 9, 0, 11, 0, 5, 0, 7, 0, 1, 0, 3, 0)
#define KS_64_3 _mm256_set_epi8(31, 0, 0, 30, 27, 0, 0, 26, 23, 0, 0, 22, 19, 0, 0, 18, 15, 0, 0, 14, 11, 0, 0, 10, 7, 0, 0, 6, 3, 0, 0, 2)
#define KS_64_4 _mm256_set_epi8(0, 28, 30, 0, 0, 24, 26, 0, 0, 20, 22, 0, 0, 16, 18, 0, 0, 12, 14, 0, 0, 8, 10, 0, 0, 4, 6, 0, 0, 0, 2, 0)

#define COMP_ZERO _mm256_set_epi32 (0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
#define COUNT64_128 _mm256_set_epi32(0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000)

#define SR2L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(30, 29, 28, 31, 26, 25, 24, 27, 22, 21, 20, 23, 18, 17, 16, 19, 14, 13, 12, 15, 10, 9, 8, 11, 6, 5, 4, 7, 2, 1, 0, 3))
#define SR3L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(29, 28, 31, 30, 25, 24, 27, 26, 21, 20, 23, 22, 17, 16, 19, 18, 13, 12, 15, 14, 9, 8, 11, 10, 5, 4, 7, 6, 1, 0, 3, 2))
#define SR4L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(28, 31, 30, 29, 24, 27, 26, 25, 20, 23, 22, 21, 16, 19, 18, 17, 12, 15, 14, 13, 8, 11, 10, 9, 4, 7, 6, 5, 0, 3, 2, 1))

/* Masks */


/* Optimization Macro for 64 blocks*/

#define SUBCELLS_64_BLOCKS_ITER(x,j)			\
  {							\
    x[3+8*j] = XOR(x[3+8*j], NOR(x[0+8*j], x[1+8*j]));	\
    x[7+8*j] = XOR(x[7+8*j], NOR(x[4+8*j], x[5+8*j]));	\
    x[1+8*j] = XOR(x[1+8*j], NOR(x[5+8*j], x[6+8*j]));	\
    x[2+8*j] = XOR(x[2+8*j], NOR(x[3+8*j], x[7+8*j]));	\
    x[6+8*j] = XOR(x[6+8*j], NOR(x[7+8*j], x[4+8*j]));	\
    x[0+8*j] = XOR(x[0+8*j], NOR(x[2+8*j], x[1+8*j]));	\
    x[4+8*j] = XOR(x[4+8*j], NOR(x[2+8*j], x[3+8*j]));	\
    x[5+8*j] = XOR(x[5+8*j], NOR(x[0+8*j], x[6+8*j]));	\
    tmp = x[0+8*j];					\
    x[0+8*j] = x[2+8*j];				\
    x[2+8*j] = x[7+8*j];				\
    x[7+8*j] = x[5+8*j];				\
    x[5+8*j] = x[1+8*j];				\
    x[1+8*j] = x[3+8*j];				\
    x[3+8*j] = x[4+8*j];				\
    x[4+8*j] = x[6+8*j];				\
    x[6+8*j] = tmp;					\
  }

#define ADD_CONSTANT_KEY_64_BLOCKS_ITER(x,j)	\
  {						\
    x[j] = XOR(x[j], rk[i][j]);			\
    x[j+1] = XOR(x[j+1], rk[i][j+1]);		\
    x[j+2] = XOR(x[j+2], rk[i][j+2]);		\
    x[j+3] = XOR(x[j+3], rk[i][j+3]);		\
  }

#define SHIFT_ROWS_64_BLOCKS_ITER(x,j)		\
  {						\
    x[8+j] = SR2L(x[8+j]);			\
    x[16+j] = SR3L(x[16+j]);			\
    x[24+j] = SR4L(x[24+j]);			\
  }

#define MIX_COLUMNS_64_BLOCKS_ITER(x,j)		\
  {						\
    tmp = x[0+j];				\
    x[8+j] = XOR(x[8+j], x[16+j]);		\
    x[16+j] = XOR(x[16+j], x[0+j]);		\
    x[0+j] = XOR(x[24+j], x[16+j]);		\
    x[24+j] = x[16+j];				\
    x[16+j] = x[8+j];				\
    x[8+j] = tmp;				\
  }

#define SUBCELLS_64_BLOCKS(x)			\
  {						\
    SUBCELLS_64_BLOCKS_ITER(x,0);		\
    SUBCELLS_64_BLOCKS_ITER(x,1);		\
    SUBCELLS_64_BLOCKS_ITER(x,2);		\
    SUBCELLS_64_BLOCKS_ITER(x,3);		\
  }

#define ADD_CONSTANT_KEY_64_BLOCKS(x)		\
  {						\
    ADD_CONSTANT_KEY_64_BLOCKS_ITER(x,0);	\
    ADD_CONSTANT_KEY_64_BLOCKS_ITER(x,4);	\
    ADD_CONSTANT_KEY_64_BLOCKS_ITER(x,8);	\
    ADD_CONSTANT_KEY_64_BLOCKS_ITER(x,12);	\
    x[22] = XOR(x[22], RC_64);			\
  }

#define SHIFT_ROWS_64_BLOCKS(x)			\
  {						\
    SHIFT_ROWS_64_BLOCKS_ITER(x,0);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,1);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,2);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,3);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,4);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,5);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,6);		\
    SHIFT_ROWS_64_BLOCKS_ITER(x,7);		\
  }

#define MIX_COLUMNS_64_BLOCKS(x)		\
  {						\
    MIX_COLUMNS_64_BLOCKS_ITER(x,0);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,1);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,2);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,3);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,4);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,5);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,6);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,7);		\
  }
/* Optimization Macro for 64 blocks*/
