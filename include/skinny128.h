#pragma once

/* 64 BLOCKS */

int skinny128_64_blocks(unsigned char *output,
			unsigned char *input,
			unsigned long long input_length,
			unsigned char *tweak,
			const unsigned char *key);
void encrypt_64_blocks(u256 x[32], u256 rk[48][16]);
void tweak_schedule_64_blocks(unsigned char *tweak,
			      u256 rk[48][16]);
void key_schedule_64_blocks(const unsigned char *key,
			    u256 rk[40][16]);
/* 64 BLOCKS */
