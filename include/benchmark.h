#pragma once

#define NUM_TIMINGS 2000

extern int skinny128_64_blocks(unsigned char *output,
			       unsigned char *input,
			       unsigned long long input_length,
			       unsigned char *tweak,
			       const unsigned char *key);

void benchmark_cipher(int blocks_number);
int cmp_dbl(const void *x, const void *y);
void get_random_imput(unsigned char *input, unsigned char *tweak, unsigned char *key, unsigned char input_length);
