# SKINNY_128_256_C_AVX_ZMAC

Implementation of the lightweight cipher skinny_64_128 in C with the AVX and AVX2 set of instructions using the ZMAC mode.
