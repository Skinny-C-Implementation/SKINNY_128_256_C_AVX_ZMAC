#include <stdio.h>
#include <string.h>

#include "avx_declaration.h"
#include "check_vectors.h"
#include "test_vectors.h"

int check_test_vector(int blocks_number){
  // Variables allocation
  unsigned long long input_length = blocks_number*16;
  unsigned char *input = malloc(input_length*sizeof(u8));
  unsigned char *output  = malloc(input_length*sizeof(u8));
  
  // Test vector
  unsigned char* plaintext;
  unsigned char* key;
  unsigned char *tweak;
  unsigned char* ciphertext;

  //Set key and test vectors
  tweak = tweak_test;
  key = key_test;
  
  if(blocks_number == 64){
    plaintext = plaintext_64;
    ciphertext = ciphertext_64;
  }
  else{
    printf("Function not implemented yet.\n");
    return 0;
  }
  
  // Duplication of the test vector for 16 blocks
  for(int i=0; i<blocks_number; i++){
    memcpy(input, plaintext, blocks_number*16);
  }
  
  // Encryption of the test vector for 16 blocks
  skinny128_64_blocks(output, input, input_length, tweak, key);
  /*
  for(int j=0; j<16; j++){
    printf("%02x ", tweak[j]);
  }
  printf("\n");
  
  for(int j=0; j<16; j++){
    printf("%02x ", key[j]);
  }
  printf("\n");
  
  for(int i=0; i<(blocks_number/2); i++){
    for(int j=0; j<32; j++){
      printf("%02x ", input[j+32*i]);
    }
    printf("\n");
  }
  printf("\n");
  */
  /*
  for(int i=0; i<(blocks_number/2); i++){
    for(int j=0; j<32; j++){
      printf("%02x ", output[j+32*i]);
    }
    printf("\n");
  }
  printf("\n");
  */
  
  // Validation
  for(int i=0; i<(blocks_number*16); i++) {
    if(output[i] != ciphertext[i]) {
      printf("ERROR: Outputstream does not match test vector at position %i!\n", i);
      free(input);
      free(output);
      return 0;
    }
  }

  //Free malloc
  free(input);
  free(output);

  return 1;
}
