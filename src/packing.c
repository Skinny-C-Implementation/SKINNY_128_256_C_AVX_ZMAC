#include "avx_declaration.h"

/* 64 BLOCKS */

void packing_64_blocks(u256 x[32], const unsigned char *input){
  u256 tmp[2];
  
  for(int i=0; i<32; i++) {
    x[i] = LOAD(input+i*32);
  }

  //Separate bits and group bytes.
  for(int i=0; i<4; i++){
    SWAPMOVE(x[8*i+0], x[8*i+1], MASK1, 1);
    SWAPMOVE(x[8*i+2], x[8*i+3], MASK1, 1);
    SWAPMOVE(x[8*i+4], x[8*i+5], MASK1, 1);
    SWAPMOVE(x[8*i+6], x[8*i+7], MASK1, 1);

    SWAPMOVE(x[8*i+0], x[8*i+2], MASK2, 2);
    SWAPMOVE(x[8*i+1], x[8*i+3], MASK2, 2);
    SWAPMOVE(x[8*i+4], x[8*i+6], MASK2, 2);
    SWAPMOVE(x[8*i+5], x[8*i+7], MASK2, 2);

    SWAPMOVE(x[8*i+0], x[8*i+4], MASK4, 4);
    SWAPMOVE(x[8*i+1], x[8*i+5], MASK4, 4);
    SWAPMOVE(x[8*i+2], x[8*i+6], MASK4, 4);
    SWAPMOVE(x[8*i+3], x[8*i+7], MASK4, 4);    
  }

  for(int i=0; i<8; i++){
    tmp[0] = UNPACKLOW32(x[i+0], x[i+16]);
    x[i+16] = UNPACKHIGH32(x[i+0], x[i+16]);
    tmp[1] = UNPACKLOW32(x[i+8], x[i+24]);
    x[i+24] = UNPACKHIGH32(x[i+8], x[i+24]);
    x[i+0] = tmp[0];
    x[i+8] = tmp[1];

    tmp[0] = UNPACKLOW32(x[i+0], x[i+8]);
    x[i+8] = UNPACKHIGH32(x[i+0], x[i+8]);
    tmp[1] = UNPACKLOW32(x[i+16], x[i+24]);
    x[i+24] = UNPACKHIGH32(x[i+16], x[i+24]);
    x[i+0] = tmp[0];
    x[i+16] = tmp[1];
  }
}

void unpacking_64_blocks(const unsigned char *output, u256 x[32]){
  u256 tmp[2];
  for(int i=0; i<8; i++){
    tmp[0] = UNPACKLOW32(x[i+0], x[i+16]);
    x[i+16] = UNPACKHIGH32(x[i+0], x[i+16]);
    tmp[1] = UNPACKLOW32(x[i+8], x[i+24]);
    x[i+24] = UNPACKHIGH32(x[i+8], x[i+24]);
    x[i+0] = tmp[0];
    x[i+8] = tmp[1];
    
    tmp[0] = UNPACKLOW32(x[i+0], x[i+8]);
    x[i+8] = UNPACKHIGH32(x[i+0], x[i+8]);
    tmp[1] = UNPACKLOW32(x[i+16], x[i+24]);
    x[i+24] = UNPACKHIGH32(x[i+16], x[i+24]);
    x[i+0] = tmp[0];
    x[i+16] = tmp[1];
  }

  for(int i=0; i<4; i++){
    SWAPMOVE(x[8*i+0], x[8*i+4], MASK4, 4);
    SWAPMOVE(x[8*i+1], x[8*i+5], MASK4, 4);
    SWAPMOVE(x[8*i+2], x[8*i+6], MASK4, 4);
    SWAPMOVE(x[8*i+3], x[8*i+7], MASK4, 4);

    SWAPMOVE(x[8*i+0], x[8*i+2], MASK2, 2);
    SWAPMOVE(x[8*i+1], x[8*i+3], MASK2, 2);
    SWAPMOVE(x[8*i+4], x[8*i+6], MASK2, 2);
    SWAPMOVE(x[8*i+5], x[8*i+7], MASK2, 2);

    SWAPMOVE(x[8*i+0], x[8*i+1], MASK1, 1);
    SWAPMOVE(x[8*i+2], x[8*i+3], MASK1, 1);
    SWAPMOVE(x[8*i+4], x[8*i+5], MASK1, 1);
    SWAPMOVE(x[8*i+6], x[8*i+7], MASK1, 1);
  }

  for(int i=0; i<32; i++) {
    STORE(output+i*32, x[i]);
  }
}

/* 64 BLOCKS */
