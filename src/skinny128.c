#include <string.h>
#include "avx_declaration.h"
#include "packing.h"
#include "skinny128.h"

const unsigned char RC[62] = {
  0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
  0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
  0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
  0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A,
  0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13,
  0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a, 0x15, 0x2a, 0x14, 0x28,
  0x10, 0x20};

/* 64 BLOCKS */

int skinny128_64_blocks(unsigned char *output,
			unsigned char *input,
			unsigned long long input_length,
			unsigned char *tweak,
			const unsigned char *key){
  u256 rk[48][16];
  u256 x[32];

  //TK2 Schedule
  key_schedule_64_blocks(key, rk);
  
  while(input_length >= 1024){
    //TK1 Schedule
    tweak_schedule_64_blocks(tweak, rk);

    //Packing
    packing_64_blocks(x, input);
    
    //Encryption
    encrypt_64_blocks(x, rk);

    //Unpacking
    unpacking_64_blocks(output, x);

    input_length -= 1024;
    input += 1024;
    output += 1024;
    tweak += 1024;
  }
  
  return 0;
}

void encrypt_64_blocks(u256 x[32], u256 rk[48][16]){
  u256 tmp;
  for(int i=0; i<48; i++){
    //Subcells
    SUBCELLS_64_BLOCKS(x);
    
    //Add constant & key
    ADD_CONSTANT_KEY_64_BLOCKS(x);
    
    //Shift Rows
    SHIFT_ROWS_64_BLOCKS(x);

    //Mix Columns
    MIX_COLUMNS_64_BLOCKS(x);
  }
}

void tweak_schedule_64_blocks(unsigned char *tweak,
			      u256 rk[48][16]){
  u256 tk1[32], tmp[2];
  
  unsigned char *tmp_tweak = (unsigned char*)malloc(64*16*sizeof(u8));
  
  //Fill TK1
  for(int i=0; i<64; i++){
    memcpy(tmp_tweak + 16*i, tweak, 16);
  }
  packing_64_blocks(tk1, tmp_tweak);

  //Extract each round tweak
    for(int j=0; j<16; j++){
      rk[0][j] = XOR(rk[0][j], tk1[j]);
      rk[0+16][j] = XOR(rk[0+16][j], tk1[j]);
      rk[0+32][j] = XOR(rk[0+32][j], tk1[j]);
    }
  
  //Fill RK
  for(int i=1; i<16; i++){    
    //Update TK1
    for(int j=0; j<8; j++){
      tmp[0] = tk1[0+j];
      tmp[1] = tk1[8+j];
      tk1[0+j] = XOR(AND(NOT(MASK8), _mm256_shuffle_epi8(tk1[16+j], KS_64_1)),
		     AND(MASK8, _mm256_shuffle_epi8(tk1[24+j], KS_64_2)));
      tk1[8+j] = XOR(AND(MASK16S, _mm256_shuffle_epi8(tk1[16+j], KS_64_3)),
		     AND(NOT(MASK16S), _mm256_shuffle_epi8(tk1[24+j], KS_64_4)));
      tk1[16+j] = tmp[0];
      tk1[24+j] = tmp[1];
    }


    //Extract each round tweak
    for(int j=0; j<16; j++){
      rk[i][j] = XOR(rk[i][j], tk1[j]);
      rk[i+16][j] = XOR(rk[i+16][j], tk1[j]);
      rk[i+32][j] = XOR(rk[i+32][j], tk1[j]);
    }
  }
  free(tmp_tweak);
}

void key_schedule_64_blocks(const unsigned char *key,
			    u256 rk[48][16]){
  u256 tk2[32], tmp[2];

  unsigned char *tmp_key = (unsigned char*)malloc(64*16*sizeof(u8));
  
  //Fill TK2
  for(int i=0; i<64; i++){
    memcpy(tmp_key + 16*i, key, 16);
  }
  packing_64_blocks(tk2, tmp_key);
  
  //Fill RK
  for(int i=0; i<48; i++){
    //Extract each round key
    for(int j=0; j<16; j++){
      rk[i][j] = tk2[j];
    }
    
    //Add constant to the key
    if(RC[i]>>5 & 1)
      rk[i][14] = XOR(rk[i][14], RC_64);
    if(RC[i]>>4 & 1)
      rk[i][15] = XOR(rk[i][15], RC_64);
    if(RC[i]>>3 & 1)
      rk[i][4] = XOR(rk[i][4], RC_64);
    if(RC[i]>>2 & 1)
      rk[i][5] = XOR(rk[i][5], RC_64);
    if(RC[i]>>1 & 1)
      rk[i][6] = XOR(rk[i][6], RC_64);
    if(RC[i]>>0 & 1)
      rk[i][7] = XOR(rk[i][7], RC_64);
    
    //Update TK2
    for(int j=0; j<8; j++){
      tmp[0] = tk2[0+j];
      tmp[1] = tk2[8+j];
      tk2[0+j] = XOR(AND(NOT(MASK8), _mm256_shuffle_epi8(tk2[16+j], _mm256_set_epi8(0,28,0,29,0,24,0,25,0,20,0,21,0,16,0,17,0,12,0,13,0,8,0,9,0,4,0,5,0,0,0,1))),
		     AND(MASK8, _mm256_shuffle_epi8(tk2[24+j], _mm256_set_epi8(29,0,31,0,25,0,27,0,21,0,23,0,17,0,19,0,13,0,15,0,9,0,11,0,5,0,7,0,1,0,3,0))));
      tk2[8+j] = XOR(AND(MASK16S, _mm256_shuffle_epi8(tk2[16+j], _mm256_set_epi8(31,0,0,30,27,0,0,26,23,0,0,22,19,0,0,18,15,0,0,14,11,0,0,10,7,0,0,6,3,0,0,2))),
		     AND(NOT(MASK16S), _mm256_shuffle_epi8(tk2[24+j], _mm256_set_epi8(0,28,30,0,0,24,26,0,0,20,22,0,0,16,18,0,0,12,14,0,0,8,10,0,0,4,6,0,0,0,2,0))));
      tk2[16+j] = tmp[0];
      tk2[24+j] = tmp[1];
    }
    //TK2 LFSR
    for(int j=0; j<2; j++){
      tmp[0] = XOR(tk2[8*j+0], tk2[8*j+2]);
      tk2[8*j+0] = tk2[8*j+1];
      tk2[8*j+1] = tk2[8*j+2];
      tk2[8*j+2] = tk2[8*j+3];
      tk2[8*j+3] = tk2[8*j+4];
      tk2[8*j+4] = tk2[8*j+5];
      tk2[8*j+5] = tk2[8*j+6];
      tk2[8*j+6] = tk2[8*j+7];
      tk2[8*j+7] = tmp[0];
    }
   
  }
  free(tmp_key);
}

/* 64 BLOCKS */
